package com.pacifica.taxe_calcul.models;

import com.pacifica.taxe_calcul.common.enumerations.ProductTypeEnum;

public class Product {

    private String libelle;
    private double unitPriceWithoutTaxe;
    private boolean isImported;
    private ProductTypeEnum productType;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getUnitPriceWithoutTaxe() {
        return unitPriceWithoutTaxe;
    }

    public void setUnitPriceWithoutTaxe(double unitPriceWithoutTaxe) {
        this.unitPriceWithoutTaxe = unitPriceWithoutTaxe;
    }

    public boolean isImported() {
        return isImported;
    }

    public void setImported(boolean imported) {
        isImported = imported;
    }

    public ProductTypeEnum getProductType() {
        return productType;
    }

    public void setProductType(ProductTypeEnum productType) {
        this.productType = productType;
    }


}
