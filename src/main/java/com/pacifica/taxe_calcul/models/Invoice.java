package com.pacifica.taxe_calcul.models;

import java.util.List;

public class Invoice {
	
	private List<InvoiceDetail> invoiceDetails;
	private double taxeAmount;
	private double totalePriceAmount;

	public List<InvoiceDetail> getInvoiceDetails() {
		return invoiceDetails;
	}

	public void setInvoiceDetails(List<InvoiceDetail> invoiceDetails) {
		this.invoiceDetails = invoiceDetails;
	}

	public double getTaxeAmount() {
		return taxeAmount;
	}

	public void setTaxeAmount(double taxeAmount) {
		this.taxeAmount = taxeAmount;
	}

	public double getTotalePriceAmount() {
		return totalePriceAmount;
	}

	public void setTotalePriceAmount(double totalePriceAmount) {
		this.totalePriceAmount = totalePriceAmount;
	}

}
