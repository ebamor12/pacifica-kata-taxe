package com.pacifica.taxe_calcul.models;

import java.util.List;

public class Order {

	private List<OrderDetail> orderDetails ;

	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Order(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Order() {
	}
}