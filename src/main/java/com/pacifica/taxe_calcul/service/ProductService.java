package com.pacifica.taxe_calcul.service;

import com.pacifica.taxe_calcul.models.Product;

public interface ProductService {
    Product createProductFromOrderLine(String orderLine, int quantity);
}
