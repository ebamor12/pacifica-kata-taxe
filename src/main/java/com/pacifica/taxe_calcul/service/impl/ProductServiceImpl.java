package com.pacifica.taxe_calcul.service.impl;

import com.pacifica.taxe_calcul.common.enumerations.ProductTypeEnum;
import com.pacifica.taxe_calcul.common.utils.ProductUtil;
import com.pacifica.taxe_calcul.models.Product;
import com.pacifica.taxe_calcul.service.ProductService;

import java.util.*;

import static com.pacifica.taxe_calcul.common.constant.CommonConstant.*;
import static com.pacifica.taxe_calcul.common.constant.ProductNameConstant.*;

public class ProductServiceImpl implements ProductService {

    public Product createProductFromOrderLine(String orderLine, int quantity) {

        Product product = new Product();
        product.setImported(orderLine.contains(IMPORT_IN_LINE_COMMAND));
        product.setLibelle(ProductUtil.getProductLibelle(product,orderLine));
        product.setUnitPriceWithoutTaxe(
                Double.valueOf(ProductUtil.substringLine(orderLine, A_IN_LINE_COMMAND, EURO_IN_LINE_COMMAND)));
        product.setProductType(this.getTypeFromProductName(product.getLibelle()));

        return product;
    }

     public ProductTypeEnum getTypeFromProductName(String label){

        ProductTypeEnum productType = null;
        Optional<ProductTypeEnum> optionalProductType = getMatchingProductNames().entrySet().stream()
                .filter(e -> e.getValue().contains(label))
                .map(Map.Entry::getKey)
                .findFirst();

        if(optionalProductType.isPresent()) {
            productType= optionalProductType.get();
        }
        return productType;
    }

    private Map<ProductTypeEnum, List<String>> getMatchingProductNames(){
        Map<ProductTypeEnum, List<String>> mapOfProductNameByType = new HashMap<>();
        mapOfProductNameByType.put(ProductTypeEnum.BOOK, Arrays.asList(PRODUCT_NAME_MANY_BOOK, PRODUCT_NAME_BOOK)) ;
        mapOfProductNameByType.put(ProductTypeEnum.FOOD,Arrays.asList(PRODUCT_NAME_MANY_CHOCOLAT, PRODUCT_NAME_CHOCOLAT, PRODUCT_NAME_CHOCOLATE_BARS, PRODUCT_NAME_CHOCOLATE_BOXES ));
        mapOfProductNameByType.put(ProductTypeEnum.MEDICATION,Arrays.asList(PRODUCT_NAME_MANY_PILL, PRODUCT_NAME_PILL, PRODUCT_NAME_PILL_BOXES ));
        mapOfProductNameByType.put(ProductTypeEnum.OTHERS,Arrays.asList(PRODUCT_NAME_PERFUME , PRODUCT_NAME_CD , PRODUCT_NAME_PERFUME_BOTTLES, PRODUCT_NAME_PERFUME_BOTTLE));

        return mapOfProductNameByType;
    }


}
