package com.pacifica.taxe_calcul.common.constant;

public class CommonConstant {
	 public static final String A_IN_LINE_COMMAND                                ="à";
	 public static final String A_ORDER                                          =" à ";
	 public static final String SPACE_STRING                                     =" ";
	 public static final String EMPTY_STRING                                     ="";
	 public static final String START_INVOICE_STRING                              ="* ";
	 public static final String EURO_IN_LINE_COMMAND                             ="€";
	 public static final String EURO_IN_INVOICE                                  ="€ : ";
	 public static final String IMPORT_IN_LINE_COMMAND                           =" import";
	 public static final String FIRST_COMMAND_FILE_PATH                          ="src/test/files/FirstInputCommande.txt";
	 public static final String THIRD_COMMAND_FILE_PATH                           ="src/test/files/ThirdInputCommande.txt";
	 public static final String SECOND_COMMAND_FILE_PATH                         ="src/test/files/SecondInputCommande.txt";
	 public static final String FIRST_EXPECTED_OUTPUT_FILE_PATH                  ="src/test/files/FirstOutputExpected.txt";
	 public static final String  TEST_PATH                                       ="src/test/files/";
	 public static final String TXT_EXTENSION                                    =".txt";
	 public static final String FILE_NAME                                        ="Facture_";
	 public static final String TAXE_TOTAL                                       ="Montant des taxes : ";
	 public static final String TOTAL_PRIX                                       ="Total : ";
	 public static final String DATE_PATTERN                                     ="yyyy-MM-dd HH-mm-ss";

	 private CommonConstant() {}
}
