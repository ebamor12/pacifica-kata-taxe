package com.pacifica.taxe_calcul.common.constant;

public class CountConstant {
    public static final int ZERO_PERCENT                                 = 0;
    public static final int TEN_PERCENT                                  = 10;
    public static final int TWENTY_PERCENT                               = 20;
    public static final int FIVE                                         = 5;
    public static final int TWO                                          = 2;
    public static final int THREE                                        = 3;
    public static final int HUNDRED                                      = 100;
    public static final double TEN_DOUBLE                                = 10D;

    private CountConstant() {
    }
}
