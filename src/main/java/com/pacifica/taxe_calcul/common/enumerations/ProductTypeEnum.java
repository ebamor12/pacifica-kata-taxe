package com.pacifica.taxe_calcul.common.enumerations;

import static com.pacifica.taxe_calcul.common.constant.CountConstant.*;

public enum ProductTypeEnum {
    FOOD(ZERO_PERCENT),
    MEDICATION(ZERO_PERCENT),
    BOOK(TEN_PERCENT),
    OTHERS(TWENTY_PERCENT);

    private final int tax;

    ProductTypeEnum(int tax) {
        this.tax = tax;
    }

    public int getTax() {
        return tax;
    }

}
